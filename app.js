const computersElement = document.getElementById("computers");
const priceElement = document.getElementById("price");
const titleElement = document.getElementById("title");
const descriptionElement = document.getElementById("description");
const salaryElement = document.getElementById("salary");
const workElement = document.getElementById("work");
const balanceElement = document.getElementById("balance");
const bankElement = document.getElementById("bank");
const getLoanElement = document.getElementById("getLoan");
const loansElement = document.getElementById("loans");
const Hiddnloanbutton = (document.getElementById("repayBtn").style.display = "none");
const loanbutton = document.getElementById("repayBtn");
const buyBtnElement = document.getElementById("buyBtn");
const compFeatueresElement = document.getElementById("comp_features");



let loans = 0;
let computers = [];
let salary = 0;
let balance = 0;

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => computers = data)
    .then(computers => addComputersToMenu(computers));


const addComputersToMenu = (computers) => {
    
    computers.forEach(x => addComputerToMenu(x))
    priceElement.innerText = computers[0].price + " NOK ";
    titleElement.innerHTML = computers[0].title;
    descriptionElement.innerHTML = computers[0].description;
    compFeatueresElement.innerText = computers[0].specs.join("\n");  
    const currentImage = document.getElementById('img')
    currentImage.setAttribute(
        'src',
        `https://noroff-komputer-store-api.herokuapp.com/${computers[0].image}`
    );
}   

const addComputerToMenu = (computer) => {
    const computerElement = document.createElement("option");
    computerElement.value = computer.id;
    computerElement.appendChild(document.createTextNode(computer.title));
    computersElement.appendChild(computerElement);

}


const handleComputerMenuChange = e =>  {
    const selectedComputer = computers[e.target.selectedIndex];
    priceElement.innerText = selectedComputer.price + " NOK";
    titleElement.innerHTML = selectedComputer.title;
    descriptionElement.innerHTML = selectedComputer.description;
    compFeatueresElement.innerText = selectedComputer.specs.join("\n");
    
    const currentImage = document.getElementById('img')

    currentImage.setAttribute(
        'src',
        `https://noroff-komputer-store-api.herokuapp.com/${selectedComputer.image}`
    );

}

const addWork = () => {
    salary += 100;
    salaryElement.innerText = `${salary}`
}


//add salary to balance 
//add 10% of salary to loans and 90% to balance
//if 10% of salary is more then existing loan the rest will be added to balance 
const addToBalance = () => {
    if(loans > 0 ){
        let mySalary=salary*0.9;
        let bankSalary=salary*0.1;

        if(bankSalary > loans){
            let restSalary = bankSalary - loans;
            loans -= loans;
            balance += restSalary + mySalary;
            balanceElement.innerText = `${balance}`
            salary = 0.0;
            salaryElement.innerText = `${salary}`
            loansElement.innerText = `Loans ${loans}`
            document.getElementById("repayBtn").style.display = "none";
        }else{
            balance += mySalary;
            loans -= bankSalary;
            balanceElement.innerText = `${balance}`
            salary = 0.0;
            salaryElement.innerText = `${salary}`
            loansElement.innerText = `Loans ${loans}`
        }
    }    
    else {
    balance += salary;
    balanceElement.innerText = `${balance}`
    salary = 0.0;
    salaryElement.innerText = `${salary}`
    }
}

//no loans if existing loan, more then 2*balance
//prompt deny string and null 
const addLoan = () => {
    if(loans > 0){
        alert("Pay your existing loan firs!")
    }else{
        let loanRequest = parseInt(prompt("How much do you want to loan?"))

        if(loanRequest > balance*2 || isNaN(loanRequest)){
        alert("Hell NO!!")
        }
        else{
         balance += loanRequest
         loans += loanRequest
         loansElement.innerText = `Loans ${loans}`
        alert(`here is you loan ${loanRequest}`)
        document.getElementById("repayBtn").style.display = "";
        }
    }
    balanceElement.innerText = `${balance}`
}

const handlePay = () =>   {
    const selectedComputer = computers[computersElement.selectedIndex];
    const computerTotal = selectedComputer.price;
  if (balance >= computerTotal) {
    balance = balance - computerTotal;
    balanceElement.innerHTML = `Balance ${balance}`;
    alert("Congratz, you bought a computer")
  } else {
    alert("Bro, you are too poor");
  }
}


//Repayment button will be showed when loan is taken and disappeared after full repayment
//if salary is more then existing loan the rest amount will be added to balance  
const repayLoans = () => {
    let rest;
  
    if (loans > 0) {
      if (salary >= loans) {
        rest = salary - loans;
        balance += rest;
        salary = 0;
        loans -= loans;
        rest = 0;
        salaryElement.innerHTML = salary;
        balanceElement.innerHTML = balance;
        loansElement.innerHTML = loans;
        document.getElementById("repayBtn").style.display = "none";
      } else if (salary < loans) {
        rest = loans - salary;
        loans = rest;
        salary = 0;
        salaryElement.innerHTML = salary;
        loansElement.innerText = `Loans ${loans}`
      }
    } else {
      alert("No loan to repay");
    }
}


computersElement.addEventListener("change", handleComputerMenuChange);
workElement.addEventListener("click",addWork);
bankElement.addEventListener("click", addToBalance);
getLoanElement.addEventListener("click", addLoan);
loanbutton.addEventListener("click", repayLoans);
buyBtnElement.addEventListener("click", handlePay);
